﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using SdlDotNet.Graphics;

namespace ImgDumper
{
    static class ImgDumper
    {
        /// <summary>
        /// Main program entry point.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        static void Main(string[] args) {
            // Print usage if not enough args were given.
            if (args.Length < 2) {
                PrintUsage();
                return;
            }


            try {
                string imagePath = args[0];
                string outputPath = args[1];
                Surface image = new Surface(imagePath);
                StreamWriter outStream = new StreamWriter(outputPath);
                ProcessImage(image, outStream);
                outStream.Close();
            } catch (Exception exception) {
                Console.Error.WriteLine("Error: " + exception.Message);
            }
        }

        /// <summary>
        /// Prints command-line usage to stdout.
        /// </summary>
        static void PrintUsage() {
            Console.Out.WriteLine(
                "Usage: imgdumper [image file] [output file]");
        }

        /// <summary>
        /// Dumps the image out to the given stream.
        /// </summary>
        /// <param name="image">Loaded image.</param>
        /// <param name="outStream">Stream to output to.</param>
        static void ProcessImage(Surface image, StreamWriter outStream) {
            Console.Out.WriteLine("Loaded image with width=" + image.Width +
                ", height=" + image.Height);
            outStream.WriteLine(image.Width);
            outStream.WriteLine(image.Height);

            for (int y = 0; y < image.Height; ++y) {
                for (int x = 0; x < image.Width; ++x) {
                    Color color = image.GetPixel(new Point(x, y));
                    outStream.WriteLine(color.R + " " + color.G + " " +
                        color.B);
                }
            }
        }
    }
}